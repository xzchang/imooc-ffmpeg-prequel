#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

#include "libavutil/avutil.h"
#include "libavdevice/avdevice.h"
#include "libavformat/avformat.h"
#include "libavcodec/packet.h"
#include "libswresample/swresample.h"

AVFormatContext* openCaptureDev(char *hwStr, int channelCnt, int sampleRate) {
    // [[vodeo device]:[audio card],[capture dev]], for more details, please refer to Capture/Alsa-FFmpeg
    int ret;
    AVInputFormat *iFmt = NULL;
    AVFormatContext *devCtx = NULL;
    AVDictionary *options = NULL;
    char channelCntStr[4]; 
    char sampleRateStr[20];
    snprintf(channelCntStr, sizeof(channelCntStr) - 1, "%d", channelCnt);
    snprintf(sampleRateStr, sizeof(sampleRateStr) - 1, "%d", sampleRate);
    
    // register audio device
    avdevice_register_all();
    
    // get input file format
    if ((iFmt = av_find_input_format("alsa")) < 0) {
        fprintf(stderr, "Can't find input format");
        exit(-1);
    }

    // those Alsa options are HW dependent. E.g. for sennheiser, SP20,
    // if we set channel_cnt == 2, it will ends up a runtime error,
    // if we set sampling rate to 48000, it will be ignored and set to 16000
    // without any warning!
    av_dict_set(&options, "channels", channelCntStr, 0);
    av_dict_set(&options, "sample_rate", sampleRateStr, 0);
    if ((ret = avformat_open_input(&devCtx, hwStr, iFmt, &options)) < 0) {
        fprintf(stderr, "Failed to open audio device, [%d]%s\n", ret, av_err2str(ret));
        exit(-1);
    }
    av_dict_free(&options);
    return devCtx;
}

SwrContext* initResampler(int inputChannelCnt, int inputSampleRate) {
    SwrContext *swrCtx = NULL;
    // channel_layout = channel_count + speaker_placement
    if (!(swrCtx = swr_alloc_set_opts(
                    NULL,                   //Context
                    AV_CH_LAYOUT_STEREO,    // output channel layout
                    AV_SAMPLE_FMT_S16,      // output bit depth
                    44100,                  // output sampling rate 
                    inputChannelCnt == 1? AV_CH_LAYOUT_MONO: AV_CH_LAYOUT_STEREO, // We are doing hackish here
                    AV_SAMPLE_FMT_S16,      // input bit depth 
                    inputSampleRate,        // input sampling rate
                    0, 
                    NULL
                    ))) {
        fprintf(stderr, "Unable to open SwResampleContext\n");
        exit(-1);
    }

    if (swr_init(swrCtx) < 0) {
        fprintf(stderr, "Unable to init SwResampleContext\n");
        exit(-1);
    }
    return swrCtx;
}

/**
 * Another good article/summary can be found here
 * https://www.programmersought.com/article/62397224750/
 * https://www.programmersought.com/article/55831435247/
 **/
int capture_audio(const char *const dest, char *hwStr, int devChannelCnt, int devSampleRate, int duration, int sndBufSz) {
    AVFormatContext *devCtx = NULL;
    int ret = 0;
    long samplesToCapture = duration * devSampleRate; // How many samples to capture to satify the duration per channel
    int count = 0;

    AVPacket *pkt = av_packet_alloc(); // Frame to read from capture device
    FILE *outputFile = fopen(dest, "wb");

    devCtx = openCaptureDev(hwStr, devChannelCnt, devSampleRate);
    SwrContext *swrCtx = initResampler(devChannelCnt, devSampleRate);

    uint8_t **inputBuffer = NULL; // input buffer for SW resampler
    int inputBufferLineSize = 0;

    uint8_t **outputBuffer = NULL; // output buffer for SW resampler
    int outputBufferLineSize = 0;

    int samplePerFrame = sndBufSz/2/devChannelCnt; // samples per frame/buffer/read_op
    av_samples_alloc_array_and_samples(
            &inputBuffer,           // allocated buffer size 
            &inputBufferLineSize,   // line size
            1,                      // channel count 
            samplePerFrame,         // How many samples per frame, depends on HW 
            AV_SAMPLE_FMT_S16,      // bitdepth 
            0                       //  
            ); 
    
    av_samples_alloc_array_and_samples(
            &outputBuffer,          // allocated buffer size 
            &outputBufferLineSize,  // line size
            2,                      // channel count 
            samplePerFrame,        
            AV_SAMPLE_FMT_S16,      // bitdepth 
            0                       //  
            ); 
    int timeOut = 0;
    int swrRet = 0;
    int pktSize = 0;
    while (!ret) {
        timeOut = (samplesToCapture < (long) count* samplePerFrame);
        if (timeOut) {
            // Read out the remaining in the resampler
            swrRet = swr_convert(swrCtx, outputBuffer, samplePerFrame, 0, 0);
            if (swrRet == 0) break; //Resampler cache is out
        } else {
            ret = av_read_frame(devCtx, pkt);
            pktSize = pkt->size;
            memcpy(inputBuffer[0], pkt->data, pkt->size); // Try to get rid of this one
            av_packet_unref(pkt);
            swrRet = swr_convert(swrCtx, outputBuffer, samplePerFrame, (const uint8_t **)inputBuffer, samplePerFrame);
            count++;
        }

        int fileWriteRet = fwrite(outputBuffer[0], outputBufferLineSize, 1, outputFile);
        printf("count = %d, sampleCnt = %d, pktSz = %d, outBufSz = %d, swrRet = %d, fileWriteRet = %d\n", 
                count, 
                pktSize/devChannelCnt/2,
                pktSize, 
                outputBufferLineSize, 
                swrRet, 
                fileWriteRet
                );
    }
    av_packet_free(&pkt);


    fflush(outputFile);
    fclose(outputFile);
    av_freep(&inputBuffer[0]);
    av_freep(&inputBuffer);
    av_freep(&outputBuffer[0]);
    av_freep(&outputBuffer);
    swr_free(&swrCtx);
    avformat_close_input(&devCtx);
    return 0;
}

int main(int argc, char* argv[])
{
    char* dst = "./captured.pcm";
    int c;
    int devChannelCnt = 1;
    int devSampleRate = 16000;
    int captureDuration = 10; // in seconds
    int sndBufSz = 256;
    char* hwStr = "hw:1";
    while ((c = getopt(argc, argv, "o:c:s:t:b:h:")) != -1) {
        switch(c) {
            case 'o':
                dst = optarg;
                break;
            case 'c':
                devChannelCnt = atoi(optarg);
                break;
            case 's':
                devSampleRate = atoi(optarg);
                break;
            case 't':
                captureDuration = atoi(optarg);
                break;
            case 'b':
                sndBufSz = atoi(optarg);
                break;
            case 'h':
                hwStr = optarg;
                break;
            default:
                fprintf(stderr, "Usage: a.out -o [dest file] -c [channel count] -s [sampling rate] -t [duration] -b [sound buffer size] -h [hw string]\n");
                sndBufSz = atoi(optarg);
                return -1;
        }
    }
    if (sndBufSz <= 0) {
        fprintf(stderr, "Usage: a.out -o [dest file] -c [channel count] -s [sampling rate] -t [duration] -b [sound buffer size] -h [hw string]\n");
        exit(-1);
    }
    printf("destFile = %s, hwString= %s, devChannelCnt = %d, devSampleRate = %d, captureDuration = %d", 
            dst,
            hwStr,
            devChannelCnt, 
            devSampleRate, 
            captureDuration
          );
    // set logger level
    av_log_set_level(AV_LOG_DEBUG);
    return capture_audio(dst, hwStr, devChannelCnt, devSampleRate, captureDuration, sndBufSz);
}
