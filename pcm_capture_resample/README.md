# For official samples, please refer to 
https://github.com/FFmpeg/FFmpeg/blob/master/doc/examples/resampling_audio.c

# We can use the following script to extract the audio stream out of a file and transcode it into he-aac-v2

# Apply HE-AAC-v2 to encode
ffmpeg -i ../../imooc-ffmpeg/killer.mp4 -vn -c:a libfdk_aac -ar 44100 -channels 2 -profile:a aac_he_v2 3.aac

# Apply HE-AAC to encode
ffmpeg -i ../../imooc-ffmpeg/killer.mp5 -vn -c:a libfdk_aac -ar 44100 -channels 2 -profile:a aac_he 2.aac

# Apply OPUS to encode
ffmpeg -i ../../imooc-ffmpeg/killer.mp4 -vn -c:a libopus  10.opus
