#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

#include "libavutil/avutil.h"
#include "libavdevice/avdevice.h"
#include "libavformat/avformat.h"
#include "libavcodec/packet.h"

/**
 * Another good article/summary can be found here
 * https://www.programmersought.com/article/62908492241/
 **/
int record_audio(const char *const dest) {
    AVFormatContext *avFmtCtx = NULL;
    AVInputFormat *iFmt = NULL;
    // [[vodep devoce]:[audio device]]
    char *devName = "hw:0";
    AVDictionary *opts = NULL;
    char errors[1024];
    int ret;


    printf("dest file = %s\n", dest);

    // register audio device
    avdevice_register_all();
    // set logger level
    av_log_set_level(AV_LOG_DEBUG);
    /**
     * List all input formats, but it's quite useless as our HW doesn't support it
     * while ((p = av_iformat_next(p)))
     *     printf("%s: %s:\n", p->name, p->long_name);
     **/

    // get input file format
    if ((iFmt = av_find_input_format("alsa")) < 0) {
        fprintf(stderr, "Can't find input format");
        exit(-1);
    }


    if ((ret = avformat_open_input(&avFmtCtx, devName, iFmt, &opts)) < 0) {
        av_strerror(ret, errors, 1024);
        fprintf(stderr, "Failed to open audio device, [%d]%s\n", ret, errors);
        exit(-1);
    }
    
    av_dump_format(avFmtCtx, 0, devName, 0);

    AVPacket *pkt = av_packet_alloc();
    int count = 0;

    FILE *out = fopen(dest, "wb");
    while (!ret && count++ < 200) {
        ret = av_read_frame(avFmtCtx, pkt);
        fwrite(pkt->data, pkt->size, 1, out);
        printf("pkt.size = %d, count = %d\n", pkt->size, count);
        av_packet_unref(pkt);
    }
    av_packet_free(&pkt);


    fflush(out);
    fclose(out);
    avformat_close_input(&avFmtCtx);
    return 0;
}

int main(int argc, char* argv[])
{
    char* dst = NULL;
    int c;
    while ((c = getopt(argc, argv, "o:")) != -1) {
        switch(c) {
            case 'o':
                dst = optarg;
                break;
        }
    }
    if (dst == NULL) {
        fprintf(stderr, "Usage: a.out -o [dest file]\n");
        return -1;
    }
    return record_audio(dst);
}
