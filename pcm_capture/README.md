# Offical doc about Alsa
https://trac.ffmpeg.org/wiki/Capture/ALSA

# List all available devices
cat /proc/asound/cards
arecord -l
aplay -L

# To utilize ffmpeg to sample audio make suer vappi is enabled, and install the proper driver for your PC
# ffplay depends on SDL2, so make sure install SDL2 before configure it
--enable-gpl --enable-nonfree --enable-libfdk-aac --enable-libx264 --enable-libx265 --enable-filter=delogo --enable-debug --disable-optimizations --enable-libspeex --enable-shared --enable-pthreads --enable-vaapi --enable-ffplay --disable-stripping --enable-debug=3 --enable-libopus

# use ldd to check ffmpeg's runtime library dependency. You can use ldconfig to alter the runtime dependencies

# Use the following scirpt to capture audio
ffmpeg -f alsa -i hw:0 out.wav -t 10

# Use the following script to capture with Sennheiser SP20
ffmpeg -f alsa -channels 1 -i hw:1 out.wav -t 10

# We can start recording audio with this programme. And we can playback the audio with the following
ffplay -ar 48000 -ac 2 -f s16le test.pcm # The sampling rate depends on the capture device, on hp 15v@ubuntu, it's 48Khz

# There's an interesting article here
https://www.programmersought.com/article/62908492241/
