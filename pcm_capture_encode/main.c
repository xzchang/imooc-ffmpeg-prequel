#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

#include "libavutil/avutil.h"
#include "libavdevice/avdevice.h"
#include "libavformat/avformat.h"
#include "libavcodec/packet.h"
#include "libswresample/swresample.h"

AVFormatContext* openCaptureDev(char *hwStr, int channelCnt, int sampleRate) {
    // [[vodeo device]:[audio card],[capture dev]], for more details, please refer to Capture/Alsa-FFmpeg
    int ret;
    AVInputFormat *iFmt = NULL;
    AVFormatContext *devCtx = NULL;
    AVDictionary *options = NULL;
    char channelCntStr[4]; 
    char sampleRateStr[20];
    snprintf(channelCntStr, sizeof(channelCntStr) - 1, "%d", channelCnt);
    snprintf(sampleRateStr, sizeof(sampleRateStr) - 1, "%d", sampleRate);
    
    // register audio device
    avdevice_register_all();
    
    // get input file format
    if ((iFmt = av_find_input_format("alsa")) < 0) {
        fprintf(stderr, "Can't find input format");
        exit(-1);
    }

    // those Alsa options are HW dependent. E.g. for sennheiser, SP20,
    // if we set channel_cnt == 2, it will ends up a runtime error,
    // if we set sampling rate to 48000, it will be ignored and set to 16000
    // without any warning!
    av_dict_set(&options, "channels", channelCntStr, 0);
    av_dict_set(&options, "sample_rate", sampleRateStr, 0);
    if ((ret = avformat_open_input(&devCtx, hwStr, iFmt, &options)) < 0) {
        fprintf(stderr, "Failed to open audio device, [%d]%s\n", ret, av_err2str(ret));
        exit(-1);
    }
    av_dict_free(&options);
    return devCtx;
}

SwrContext* initResampler(int inputChannelCnt, int inputSampleRate) {
    SwrContext *swrCtx = NULL;
    // channel_layout = channel_count + speaker_placement
    if (!(swrCtx = swr_alloc_set_opts(
                    NULL,                   //Context
                    AV_CH_LAYOUT_STEREO,    // output channel layout
                    AV_SAMPLE_FMT_S16,      // output bit depth
                    44100,                  // output sampling rate 
                    inputChannelCnt == 1? AV_CH_LAYOUT_MONO: AV_CH_LAYOUT_STEREO, // We are doing hackish here
                    AV_SAMPLE_FMT_S16,      // input bit depth 
                    inputSampleRate,        // input sampling rate
                    0, 
                    NULL
                    ))) {
        fprintf(stderr, "Unable to open SwResampleContext\n");
        exit(-1);
    }

    if (swr_init(swrCtx) < 0) {
        fprintf(stderr, "Unable to init SwResampleContext\n");
        exit(-1);
    }
    return swrCtx;
}

AVCodecContext* initEncoder() {
    // AVCodec* codec = avcodec_find_encoder(AV_CODEC_ID_OPUS); //AV_CODEC_ID_AAC
    AVCodec* codec = avcodec_find_encoder_by_name("libfdk_aac"); //AV_CODEC_ID_AAC
    AVCodecContext* codecCtx = avcodec_alloc_context3(codec);
    codecCtx->sample_fmt = AV_SAMPLE_FMT_S16;
    codecCtx->channel_layout = AV_CH_LAYOUT_STEREO;
    codecCtx->sample_rate = 48000;
    codecCtx->channels = 2;
    // codecCtx->bit_rate = 64000; //AAC_LA: 128K, HE_AAC: 64K, HE_AAC_V2: 32K
    codecCtx->bit_rate = 0; // Set to 0 so that profile applies
    codecCtx->profile = FF_PROFILE_AAC_HE_V2;
    if (avcodec_open2(codecCtx, codec, NULL) < 0) {
        fprintf(stderr, "avcodec_open2() failed\n");
        exit(-1);
    }
    return codecCtx;
}

AVFrame* initEncoderFrame(int samplesPerFrame) {
    AVFrame *frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "av_frame_alloc() failed\n");
        exit(-1);
    }
    frame->nb_samples = samplesPerFrame;
    frame->format = AV_SAMPLE_FMT_S16;
    frame->channel_layout = AV_CH_LAYOUT_STEREO;
    av_frame_get_buffer(frame, 0);
    if (!frame->data[0]) {
        fprintf(stderr, "av_frame_get_buffer() failed\n");
        exit(-1);
    }
    return frame;
}

AVPacket* initEncoderPacket() {
    AVPacket *newpkt = av_packet_alloc();
    if (!newpkt) {
        fprintf(stderr, "av_packet_alloc() failed\n");
        exit(-1);
    }
    return newpkt;
}

void encode(AVCodecContext *encoderCtx, AVFrame *frame, AVPacket *packet, FILE *outputFile) {
    // Start of encode
    int ret = avcodec_send_frame(encoderCtx, frame);
    if (ret < 0) {
        fprintf(stderr, "avcodec_send_frame() returns %d", ret);
    }
    // Try to read out the encoder
    while (ret >= 0) {
        // In case of success, try to read out the encoder
        ret = avcodec_receive_packet(encoderCtx, packet);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            return;
        } else if (ret < 0) {
            fprintf(stderr, "Encoding error! avcodec_receive_packet() returns minus value\n");
            exit(-1);
        } // else {} optimistic path, ignore
        fwrite(packet->data, packet->size, 1, outputFile);
    }
    return;
}

/**
 * Another good article/summary can be found here
 * https://www.programmersought.com/article/62397224750/
 * https://www.programmersought.com/article/55831435247/
 **/
int capture_audio(const char *const dest, char *hwStr, int devChannelCnt, int devSampleRate, int duration, int sndBufSz) {
    int ret = 0;
    long samplesToCapture = duration * devSampleRate; // How many samples to capture to satify the duration per channel
    int count = 0;
    int samplesPerFrame = sndBufSz/2/devChannelCnt; // samples per frame/buffer/read_op

    AVPacket *pkt = av_packet_alloc(); // Frame to read from capture device
    FILE *outputFile = fopen(dest, "wb");

    // Init file shell handler
    AVFormatContext *devCtx = openCaptureDev(hwStr, devChannelCnt, devSampleRate);
    // Init resampler
    SwrContext *swrCtx = initResampler(devChannelCnt, devSampleRate);
    // Init encoder
    AVCodecContext *encoderCtx = initEncoder();
    // Init encoder input frame
    AVFrame *frame = initEncoderFrame(samplesPerFrame);
    // Init encoder output packet
    AVPacket *newpkt = initEncoderPacket();

    // Init resampler input buffer
    uint8_t **resamplerInBuf = NULL; 
    int resamplerInBufLineSize = 0;

    av_samples_alloc_array_and_samples(
            &resamplerInBuf,           // allocated buffer size 
            &resamplerInBufLineSize,   // line size
            1,                      // channel count 
            samplesPerFrame,         // How many samples per frame, depends on HW 
            AV_SAMPLE_FMT_S16,      // bitdepth 
            0                       //  
            ); 

    // Init resampler output buffer
    uint8_t **resamplerOutBuf = NULL;
    int resamplerOutBufLineSize = 0;
    
    av_samples_alloc_array_and_samples(
            &resamplerOutBuf,          // allocated buffer size 
            &resamplerOutBufLineSize,  // line size
            2,                      // channel count 
            samplesPerFrame,        
            AV_SAMPLE_FMT_S16,      // bitdepth 
            0                       //  
            ); 
    int timeOut = 0;
    int swrRet = 0;
    int pktSize = 0;
    while (!ret) {
        timeOut = (samplesToCapture < (long) count* samplesPerFrame);
        if (timeOut) {
            // Read out the remaining in the resampler
            swrRet = swr_convert(swrCtx, resamplerOutBuf, samplesPerFrame, 0, 0);
            if (swrRet == 0) break; //Resampler cache is out
            pktSize = 0;
        } else {
            ret = av_read_frame(devCtx, pkt);
            pktSize = pkt->size;
            memcpy(resamplerInBuf[0], pkt->data, pkt->size); // Try to get rid of this one
            av_packet_unref(pkt);
            swrRet = swr_convert(swrCtx, resamplerOutBuf, samplesPerFrame, (const uint8_t **)resamplerInBuf, samplesPerFrame);
            count++;
        }

        memcpy(frame->data[0], resamplerOutBuf[0], resamplerOutBufLineSize);
        encode(encoderCtx, frame, newpkt, outputFile);

        printf("count = %d, sampleCnt = %d, pktSz = %d, outBufSz = %d, swrRet = %d\n", 
                count, 
                pktSize/devChannelCnt/2,
                pktSize, 
                resamplerOutBufLineSize, 
                swrRet 
                );
    }
    // Read out the reaming in the encoder cache
    encode(encoderCtx, NULL, newpkt, outputFile);
    if (frame){
        av_frame_free(&frame);
    }
    if (newpkt) {
        av_packet_free(&newpkt);
    }
    if (pkt) {
        av_packet_free(&pkt);
    }

    fflush(outputFile);
    fclose(outputFile);
    if (resamplerInBuf) {
        av_freep(&resamplerInBuf[0]);
    }
    av_freep(&resamplerInBuf);
    if (resamplerOutBuf) {
        av_freep(&resamplerOutBuf[0]);
    }
    av_freep(&resamplerOutBuf);
    swr_free(&swrCtx);
    avformat_close_input(&devCtx);
    return 0;
}

int main(int argc, char* argv[])
{
    char* dst = "./captured.aac";
    int c;
    int devChannelCnt = 1;
    int devSampleRate = 16000;
    int captureDuration = 10; // in seconds
    int sndBufSz = 256;
    char* hwStr = "hw:1";
    while ((c = getopt(argc, argv, "o:c:s:t:b:h:")) != -1) {
        switch(c) {
            case 'o':
                dst = optarg;
                break;
            case 'c':
                devChannelCnt = atoi(optarg);
                break;
            case 's':
                devSampleRate = atoi(optarg);
                break;
            case 't':
                captureDuration = atoi(optarg);
                break;
            case 'b':
                sndBufSz = atoi(optarg);
                break;
            case 'h':
                hwStr = optarg;
                break;
            default:
                fprintf(stderr, "Usage: a.out -o [dest file] -c [channel count] -s [sampling rate] -t [duration] -b [sound buffer size] -h [hw string]\n");
                sndBufSz = atoi(optarg);
                return -1;
        }
    }
    if (sndBufSz <= 0) {
        fprintf(stderr, "Usage: a.out -o [dest file] -c [channel count] -s [sampling rate] -t [duration] -b [sound buffer size] -h [hw string]\n");
        exit(-1);
    }
    printf("destFile = %s, hwString= %s, devChannelCnt = %d, devSampleRate = %d, captureDuration = %d", 
            dst,
            hwStr,
            devChannelCnt, 
            devSampleRate, 
            captureDuration
          );
    // set logger level
    av_log_set_level(AV_LOG_DEBUG);
    return capture_audio(dst, hwStr, devChannelCnt, devSampleRate, captureDuration, sndBufSz);
}
