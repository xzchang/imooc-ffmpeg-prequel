#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

#include "libavutil/avutil.h"
#include "libavdevice/avdevice.h"
#include "libavformat/avformat.h"
#include "libavcodec/packet.h"

int capture_video(const char *const dest) {
    AVFormatContext *avFmtCtx = NULL;
    AVInputFormat *iFmt = NULL;
    char *devName = "/dev/video0";
    AVDictionary *opts = NULL;
    // Those options, "framerate", "video_size" and "input_format" are specificed by video4linux2 component
    // Please consult the video4linux2 doc
    av_dict_set(&opts, "framerate", "30", 0);
    av_dict_set(&opts, "video_size", "640x360", 0);
    av_dict_set(&opts, "input_format", "yuyv422", 0);
    char errors[1024];
    int ret;


    printf("dest file = %s\n", dest);

    // register video device
    avdevice_register_all();
    // set logger level
    av_log_set_level(AV_LOG_DEBUG);
    
    // List all suported input format, both video format and video format
    while ((iFmt = av_iformat_next(iFmt))) // format "video4linux" is one of them
        printf("%s: %s:\n", iFmt->name, iFmt->long_name);


    // get input format
    if ((iFmt = av_find_input_format("video4linux")) < 0) {
        fprintf(stderr, "Can't find input format");
        exit(-1);
    }


    if ((ret = avformat_open_input(&avFmtCtx, devName, iFmt, &opts)) < 0) {
        av_strerror(ret, errors, 1024);
        fprintf(stderr, "Failed to open video device, [%d]%s\n", ret, errors);
        exit(-1);
    }
    
    av_dump_format(avFmtCtx, 0, devName, 0);

    AVPacket *pkt = av_packet_alloc();
    int count = 0;

    FILE *out = fopen(dest, "wb");
    while (!ret && count++ < 200) {
        ret = av_read_frame(avFmtCtx, pkt);
        fwrite(pkt->data, pkt->size, 1, out);
        printf("pkt.size = %d, count = %d\n", pkt->size, count);
        av_packet_unref(pkt);
    }
    av_packet_free(&pkt);


    fflush(out);
    fclose(out);
    avformat_close_input(&avFmtCtx);
    return 0;
}

int main(int argc, char* argv[])
{
    char* dst = NULL;
    int c;
    while ((c = getopt(argc, argv, "o:")) != -1) {
        switch(c) {
            case 'o':
                dst = optarg;
                break;
        }
    }
    if (dst == NULL) {
        fprintf(stderr, "Usage: a.out -o [dest file]\n");
        return -1;
    }
    return capture_video(dst);
}
