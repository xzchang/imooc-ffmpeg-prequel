# install video4linux2 dependencies, so that ffmpeg can utilize the libraries
sudo apt-get install libv4l-dev
sudo apt-get install v4l-utils

# configure ffmpeg with v4l2 options
./configure --enable-libv4l2 --enable-zlib --enable-vaapi --enable-pthreads --enable-vdpau --enable-xlib --enable-libv4l2

# consulting the video4linux2. The following says the camera supports two types of output
# 1. YUV422
# 2. mjpeg
chang@840g3:~/git-root/imooc-ffmpeg/chapter3_ffmpeg_shell$ ~/git-root/ffmpeg-min/ffplay -f video4linux2 -list_formats all /dev/video0
ffplay version 4.4 Copyright (c) 2003-2021 the FFmpeg developers
  built with gcc 9 (Ubuntu 9.3.0-17ubuntu1~20.04)
  configuration: --enable-gpl --enable-nonfree --enable-libfdk-aac --enable-libx264 --enable-libx265 --enable-filter=delogo --enable-debug --disable-optimizations --enable-libspeex --enable-shared --enable-pthreads --enable-vaapi --enable-ffplay --disable-stripping --enable-debug=3 --enable-libopus --enable-libv4l2 --enable-zlib --enable-vaapi --enable-pthreads --enable-vdpau --enable-xlib --enable-libv4l2
  libavutil      56. 70.100 / 56. 70.100
  libavcodec     58.134.100 / 58.134.100
  libavformat    58. 76.100 / 58. 76.100
  libavdevice    58. 13.100 / 58. 13.100
  libavfilter     7.110.100 /  7.110.100
  libswscale      5.  9.100 /  5.  9.100
  libswresample   3.  9.100 /  3.  9.100
  libpostproc    55.  9.100 / 55.  9.100
[video4linux2,v4l2 @ 0x7fdbd4000c80] Raw       :     yuyv422 :           YUYV 4:2:2 : 640x480 320x180 320x240 424x240 640x360
[video4linux2,v4l2 @ 0x7fdbd4000c80] Compressed:       mjpeg :          Motion-JPEG : 640x480 320x180 320x240 424x240 640x360 848x480 960x540 1280x720
/dev/video0: Immediate exit requested

# Use the following command to play the video
ffplay -s 640x360 -pix_fmt yuyv422 ./hello_camera.yuv
